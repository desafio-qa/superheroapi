# Projeto Desafio
Testes de serviço para validar requests da API SuperHero.
# **Requisitos**
- [node v12.18.3](https://nodejs.org/pt-br/download/)
- npm v6.14.6
# Instalando dependências
- npm install -g newman

# Executando em linha de comando

``` newman run postman_collection.json --environment postman_environment.json ```

# Executando com Docker

``` docker build -t postman-test . ```
``` docker run postman-test ```

# Futuras implementações

- Utilização do docker na pipeline.
