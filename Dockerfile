FROM node:12

RUN mkdir /app
WORKDIR /app

COPY postman_collection.json /app/postman_collection.json
COPY postman_enviroment.json /app/postman_enviroment.json

RUN npm install -g newman

CMD newman run postman_collection.json --environment postman_enviroment.json
